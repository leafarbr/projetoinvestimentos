package br.com.itau;

import java.util.Scanner;

public class Main {


    public static void main(String[] args) {
	// Imprime a Pergunta

        int codigo;
        double valor;
        int prazo;
        double taxa;

        Scanner scaner = new Scanner(System.in);
        System.out.println("SISTEMA DE FINANCIAMENTO");
        System.out.println("------------------------");
        System.out.println("ESCOLHA O PRODUTO:");

        System.out.print("1 - CDB; 2 - LCA: ");
        codigo = scaner.nextInt();

        System.out.print("ESCOLHA O VALOR: ");
        valor = scaner.nextInt();

        System.out.print("ESCOLHA O PRAZO: ");
        prazo = scaner.nextInt();

        Produto produto = new Produto(codigo);
        Calculo calculo = new Calculo(valor,prazo, produto.getTaxa());

        System.out.println("RESULTADO ESPERAD0");

        System.out.print("VALOR: " + String.format("%.2f",calculo.getResultado()));

    }
}
