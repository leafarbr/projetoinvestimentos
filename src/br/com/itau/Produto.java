package br.com.itau;

public class Produto {

    private int codigo;
    private String descricao;
    private double taxa;

    public int getCodigo() {
        return codigo;
    }

    public String getDescricao() {
        return descricao;
    }

    public double getTaxa() {
        return taxa;
    }

    public Produto(int pcodigo) {

        codigo = pcodigo;

        switch (codigo) {
            case 1:
                codigo = 1;
                descricao = "CDB";
                taxa = 0.07;
                break;
            case 2:
                codigo = 1;
                descricao = "LCA";
                taxa = 0.08;
                break;
            default:
                codigo = 0;
                descricao = "Produto não localizado";
                taxa = 0.08;
                break;
        }
    }

}

